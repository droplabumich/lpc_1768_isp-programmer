

#ifndef _PROGRAMMER_H_INCLUDED // is myheader.h already included?
#define _PROGRAMMER_H_INCLUDED // define this so we know it's included

#include <iostream>
#include "isp.h"

class Programmer: public ISP {
public:
	Programmer(const char *portname, int speed);

	bool resetTarget();
private:
	bool Target2ISP();

};

#endif 
