#include<cstdlib>

#include <zconf.h>
#include "Serial.h"

class ISP {

public:

    //ISP();
    ISP(const char *portname, int speed);
    ~ISP();

    unsigned int getID(void);
    float getBootCode(void);
    unsigned int getSerialNum(int* sn1,int* sn2,int* sn3,int* sn4);
    int CheckHexFile(char *filename);
    int FlashHexFile(char *filename, int lastsector);
    int synchronize();
    virtual bool resetTarget() = 0;
    void printProgressBar(float &progress); 

    SerialPort port;

private:

    void TargetSendString(char* in_str);
    void TargetSendStringAndCR(char* in_str);
    int TargetReceiveString(char *strbuf, int bufsize, int timeoutms);
    virtual bool Target2ISP() = 0;
    int GetLastSector(int lastaddress);
    int ProgramSector(int offset, unsigned char bytebuf[], int lastsector);
    void uuencode(unsigned char * src, unsigned char * dest);
    int StrToHex(char *pstr, int len);
    int CheckLine(const int &rectype, const int &addr, const int &reclen);

    uint32_t numlines; 

    char strUnlock[8];
    char str0[2];
    char strJ[2];
    char strK[2];
    char strN[2];
    char strNULL[1];
    char strsync[13];
    char strok[3];
    char str12[6];

    unsigned char bytebuf[1500];

    bool synchronized;
};
