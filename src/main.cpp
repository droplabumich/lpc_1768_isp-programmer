#include <iostream>
#include "../include/programmer.h"
#include <iomanip>
#include "string.h"
#include <unistd.h>

int main(int argc, char **argv) {

  int fflag = 0;
  int pflag = 0;
  int sflag = 0; 
  char *filename = NULL;
  char *port = NULL; 
  int index;
  int c; 
  int speed; 

  opterr = 0;



  while ((c = getopt (argc, argv, "f:p:s:")) != -1)
    switch (c)
      {
      case 'f':
        fflag = 1;
	filename = optarg;
	std::cout << "File option set: "<< filename <<std::endl;
        break;
      case 'p':
        pflag = 1;
	port = optarg;
        break;
      case 's': {
	sflag = 1; 
        char* endp = NULL;
     	long l = -1;
	//Check for correct input 
     	if (!optarg ||  ((l=strtol(optarg, &endp,0)),(endp && *endp))) {
		fprintf(stderr, "invalid -s option '%s' - expecting a number\n", 
                 optarg?optarg:"");
         	exit(EXIT_FAILURE);
       	};
      	//Convert to variable
      	speed = (int) l;
        break;
	}
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort ();
      }


    if (!sflag) {
    	std::cout << "You need to specify a speed for the serial port" << std::endl;
        exit(0);
    }
    if (!pflag) {
	   std::cout << "You need to specify a serial port" << std::endl;
       exit(0);
    }
    if (!fflag) { 
        std::cout << "You need to specify a file to upload" <<std::endl;
        exit(0);
    }

    int LPC1768_id = 0x26013F37;
	

    int sn1,sn2,sn3,sn4;
    Programmer isp(port,speed);
  
    //char filename[] = "/home/odroid/odroid-development/lpc_1768_isp-programmer/outputfile.hex";
    int last_sector = isp.CheckHexFile(filename);
    if (last_sector == -1) {
        std::cout << "Error parsing the file"<< std::endl;
        exit(0);
    }
    else {
        std::cout << "Valid Hex file loaded with last sector at: " << last_sector << std::endl;
    }


    if (isp.port.connected()) {
        sleep(0.5);

        isp.synchronize();

        int device_id = isp.getID();
        if (device_id != LPC1768_id){
            std::cout << "No device with correct id found" << std::endl;
            exit(0);
        }
        std::cout<<"Valid Device ID: Ox"<<std::hex << device_id<<std::dec<<std::endl;
        isp.getBootCode();
        isp.getSerialNum(&sn1,&sn2,&sn3,&sn4);
        std::cout<<std::hex<<"Serial Number: "<<sn1<<"-"<<sn2<<"-"<<sn3<<"-"<<sn4<<std::dec<<std::endl;

        int prog_error = isp.FlashHexFile(filename,last_sector);
        std::cout << "Programming error: "<< prog_error <<std::endl;
	isp.resetTarget();
    }

    return 0;
}
