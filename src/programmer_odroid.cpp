#include <wiringPi.h>
#include "programmer.h"


Programmer::Programmer(const char *portname="/dev/ttyACM0", int speed = 115200) : ISP(portname,speed) {
		wiringPiSetup();
		std::cout << "Programmer Odroid XU4 hardware interface"<<std::endl;				
}

bool Programmer::resetTarget() {
	int pinReset = 7; 
		
	std::cout << "Resetting..."<<std::endl;	
				
	pinMode(pinReset,INPUT);
	pullUpDnControl(pinReset,PUD_UP);
	sleep(1.0);
	pinMode(pinReset,OUTPUT); 
	
	digitalWrite(pinReset,LOW); 
	
	sleep(1.0);
	digitalWrite(pinReset,HIGH);
	sleep(1.0); 
	pinMode(pinReset,INPUT); 
	pullUpDnControl(pinReset,PUD_UP);

	std::cout << "Resetted:"<<std::endl;
	return true;
}

bool Programmer::Target2ISP() {

	int pinReset = 7; 
	int pinBoot = 2; 
	std::cout << "Resetting..."<<std::endl;	
		
	pinMode(pinBoot,INPUT);
	pullUpDnControl(pinBoot,PUD_UP);
	pinMode(pinReset,INPUT);
	pullUpDnControl(pinReset,PUD_UP);
	sleep(1.0);
	pinMode(pinReset,OUTPUT); 
	pinMode(pinBoot,OUTPUT);	
	digitalWrite(pinReset,LOW); 
	digitalWrite(pinBoot,LOW); 
	sleep(1.0);
	digitalWrite(pinReset,HIGH);
	sleep(1.0); 
	pinMode(pinReset,INPUT); 
	pullUpDnControl(pinReset,PUD_UP);
	pinMode(pinBoot,INPUT); 
	pullUpDnControl(pinBoot,PUD_UP);
	
	std::cout << "Resetted:    "<<std::endl;
	return true;
}



