#include <iostream>
#include "programmer.h"


Programmer::Programmer(const char *portname="/dev/ttyACM0", int speed = 115200) : ISP(portname,speed) {
    std::cout << "Programmer blank hardware interface"<<std::endl;				
}

bool Programmer::resetTarget() {	
	std::cout << "Reset the target..."<<std::endl;				
	return true;
}

bool Programmer::Target2ISP() {		
	std::cout << "Put the target into ISP mode"<<std::endl;				
	return true;
}

