

#include "../include/isp.h"
#include "string.h"
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <ctime>


/*ISP::ISP() {
    static char const strUnlock_data[] = "U 23130";
    static char const str0_data[] = "0";
    static char const str12_data[] = "12000";
    static char const strJ_data[] = "J";
    static char const strK_data[] = "K";
    static char const strN_data[] = "N";
    static char const strNULL_data[] = "";
    static char const strsync_data[] = "Synchronized";
    static char const strok_data[] = "OK";

    strcpy( strUnlock, strUnlock_data );
    strcpy( str0, str0_data );
    strcpy( str12, str12_data );
    strcpy( strJ, strJ_data );
    strcpy( strK, strK_data );
    strcpy( strN, strN_data );
    strcpy( strNULL, strNULL_data );
    strcpy( strsync, strsync_data );
    strcpy( strok, strok_data );

    numlines = 0;
    synchronized = false;
    hw_iface::init();
    bool portsuc = port.connect("/dev/ttyACM0",115200);


}*/

ISP::ISP(const char *portname="/dev/ttyACM0", int speed = 115200) {
    static char const strUnlock_data[] = "U 23130";
    static char const str0_data[] = "0";
    static char const str12_data[] = "12000";
    static char const strJ_data[] = "J";
    static char const strK_data[] = "K";
    static char const strN_data[] = "N";
    static char const strNULL_data[] = "";
    static char const strsync_data[] = "Synchronized";
    static char const strok_data[] = "OK";

    strcpy( strUnlock, strUnlock_data );
    strcpy( str0, str0_data );
    strcpy( str12, str0_data );
    strcpy( strJ, strJ_data );
    strcpy( strK, strK_data );
    strcpy( strN, strN_data );
    strcpy( strNULL, strNULL_data );
    strcpy( strsync, strsync_data );
    strcpy( strok, strok_data );

    synchronized = false;
    //hw_iface::init();
    numlines = 0; 
    bool portsuc = port.connect(portname,speed);

}

ISP::~ISP() {
  port.disconnect();
    std::cout << "Port disconnected" << std::endl;
};

void ISP::printProgressBar(float &progress) {
    int barWidth = 70;

    std::cout << "[";
    int pos = barWidth * progress;
    for (int i = 0; i < barWidth; ++i) {
        if (i < pos) std::cout << "=";
        else if (i == pos) std::cout << ">";
        else std::cout << " ";
    }
    std::cout << "] " << int(progress * 100.0) << " %\r";
    std::cout.flush();
}

int ISP::StrToHex(char *pstr, int len)
{
    int val = 0;
    int i;
    
    for ( i = 0 ; i < len ; i++ )
    {
        if ( pstr[i] >= '0' && pstr[i] <= '9' )
            val = val * 16 + pstr[i] - '0';
        else if ( pstr[i] >= 'A' && pstr[i] <= 'F' )
            val = val * 16 + pstr[i] - 'A' + 10;
        else if ( pstr[i] >= 'a' && pstr[i] <= 'f' )
            val = val * 16 + pstr[i] - 'A' + 10;
        else
            val = val * 16;
    }
    return val;
}

int ISP::GetLastSector(int lastaddress)
{
    if ( lastaddress <= 0xFFFF )
        return (lastaddress)/4096;
    return (lastaddress)/32768-2+16;
}

void ISP::uuencode(unsigned char * src, unsigned char * dest)
{
    int i;
    
    dest[0] = (src[0] >> 2 )+ 0x20;
    dest[1] = ((src[0]&0x03)<<4) + (src[1] >> 4 )+ 0x20;
    dest[2] = ((src[1]&0x0F)<<2) + (src[2] >> 6 )+ 0x20;
    dest[3] = (src[2] & 0x3F )+ 0x20;
    for ( i = 0 ; i < 4 ; i++ )
        if ( dest[i] == 0x20 )
            dest[i] = 0x60;
}

int ISP::synchronize() {
    char buf[128];

    int buflen = 0;
        std::cout << "Synchronizing" <<std::endl;
    Target2ISP();

    char interr[] ="?";
    TargetSendString(interr);
    buflen = TargetReceiveString(buf, 128, 1000);
    if (strcmp(buf,strsync) != 0 )
        return 0;

    TargetSendStringAndCR(strsync);
    buflen = TargetReceiveString(buf, 128, 1000);
    if ( strcmp(buf,strsync) != 0 )
        return 0;
    buflen = TargetReceiveString(buf, 128, 1000);
    if ( strcmp(buf,strok) != 0 )
        return 0;

    TargetSendStringAndCR(str12);
    buflen = TargetReceiveString(buf, 128, 1000);
    if ( strcmp(buf,str12) != 0 )
        return 0;
    buflen = TargetReceiveString(buf, 128, 1000);
    if ( strcmp(buf,strok) != 0 )
        return 0;

    synchronized = true;

    return 1;
}

unsigned int ISP::getID(void)
{
    if (!synchronized) {
        std::cout << "Target not synchronized!" <<std::endl;
        return 0;
    }

    char buf[128];
    int buflen;
    unsigned int id=0;

    TargetSendStringAndCR(strJ);
    buflen = TargetReceiveString(buf, 128, 1000);
    if ( strcmp(buf,strJ) != 0 )
        return 0;    
    buflen = TargetReceiveString(buf, 128, 1000);
    if ( strcmp(buf,str0) != 0 )
        return 0;    
    buflen = TargetReceiveString(buf, 128, 1000);
    for (int i = 0 ; i < buflen ; i++ )
        id = id * 10 + buf[i] - '0';
    //TargetSendString(strNULL);       // to clear receiving buffer
    
    return id;
}

float ISP::getBootCode(void) {

    if (!synchronized) {
        std::cout << "Target not synchronized!" <<std::endl;
        return 0;
    }

    char buf[128] = {0};
    int buflen;

    TargetSendStringAndCR(strK);
    buflen = TargetReceiveString(buf, 128, 1000);
    if ( strcmp(buf,strK) != 0 )
        return 0;
    buflen = TargetReceiveString(buf, 128, 1000);
    if ( strcmp(buf,str0) != 0 )
        return 0;
    buflen = TargetReceiveString(buf, 128, 1000);
    int minor_ver = atoi(buf);
    buflen = TargetReceiveString(buf, 128, 1000);
    int major_ver = atoi(buf);
    float bootversion = (float) major_ver + ((float) minor_ver)/10.0;
    //TargetSendString(strNULL);       // to clear receiving buffer

    return bootversion;
}

unsigned int ISP::getSerialNum(int* sn1,int* sn2,int* sn3,int* sn4) {

    if (!synchronized) {
        std::cout << "Target not synchronized!" <<std::endl;
        return 0;
    }

    char buf[128] = {0};
    int buflen;
    unsigned int bootversion=0;

    TargetSendStringAndCR(strN);
    buflen = TargetReceiveString(buf, 128, 1000);
    if ( strcmp(buf,strN) != 0 )
        return 0;
    buflen = TargetReceiveString(buf, 128, 1000);
    if ( strcmp(buf,str0) != 0 )
        return 0;

    buflen = TargetReceiveString(buf, 128, 1000);
    *sn1 = atoi(buf);
    buflen = TargetReceiveString(buf, 128, 1000);
    *sn2 = atoi(buf);
    buflen = TargetReceiveString(buf, 128, 1000);
    *sn3 = atoi(buf);
    buflen = TargetReceiveString(buf, 128, 1000);
    *sn4 = atoi(buf);



    return bootversion;
}

int ISP::CheckHexFile(char *filename)
{
    int error = 0;
    int reclen, addr, rectype, wChkSum, lastaddr=0;
    uint32_t extendedAddr = 0; 
    int i;
    FILE *fp;
    char buf[128];

    fp = fopen(filename, "r");
    if (fp == NULL) {
	std::cout << "Could not open port" <<std::endl;
        return -1;
    }
        
    while (fgets(buf, sizeof(buf), fp) != NULL) 
    {
        numlines++; 
        if ( buf[0] != ':' )
        {
            std::cout <<" Invalid start "<<std::endl;
            error = -1;
        }
        reclen = StrToHex(buf+1,2);
        addr = StrToHex(buf+3,4);
        rectype = StrToHex(buf+7,2);
        error = CheckLine(rectype,addr,reclen);
        int wChkSum = 0;
        for ( i = 0 ; i < (reclen + 5) ; i++ ) {
            wChkSum += StrToHex(buf+1+i*2, 2);
        }
        if ( (wChkSum & 0xFF) != 0 ) {
            std::cout << " invalid checksum" <<std::endl;
            error = -6;
        }
        if ( rectype == 0 )
        {
            //std::cout <<addr+ extendedAddr<<std::endl;
            addr += extendedAddr;
            if ( lastaddr <= (addr + reclen) )
                lastaddr = (addr + reclen);
            else
                error = -1;
        }
        if (rectype == 2 ) {
            extendedAddr = StrToHex(buf+9, 4)<<4;
            std::cout << "Extended Address: "<<extendedAddr <<std::endl;

        }
    }
    fclose(fp);
    std::cout <<"Error: "<<error<<std::endl;
    if ( error == 0 )
        return GetLastSector(lastaddr);
    return -1;
}

int ISP::ProgramSector(int offset, unsigned char bytebuf[], int lastsector)
{

    if (!synchronized) {
        std::cout << "Target not synchronized!" <<std::endl;
        return -1;
    }

    int i, j, num, buflen;
    unsigned int chksum;
    char buf[128], sendstr[64];
    int error = 0;
    
    if ( offset == 0 )
    {
                    chksum = 0;
                    for ( i = 0 ; i < 0x1C ; i+=4 )
                    {
                        chksum += *(unsigned int *)(bytebuf+i);
                    }
                    *(unsigned int *)(bytebuf+0x1C) = 0xFFFFFFFF - chksum + 1;
    }
    chksum = 0;
    for ( i = 0 ; i < 512 ; i++ )
        chksum += bytebuf[i];
    //write to RAM address 10000200h, 512 bytes
    strcpy(sendstr,"W 268435968 512");
    TargetSendStringAndCR(sendstr);
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,sendstr) != 0 )
        error = -1;    
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,str0) != 0 )
        error = -1;
    // uuencode of the first 512 bytes data
    for ( i = 0 ; i < 512 ; i+=45 )
    {   // max 45 bytes a time
        num = 512 - i;
        if ( num > 45 )
            num = 45;
        sendstr[0] = num + 0x20;
        for ( j = 0 ; j < num ; j+= 3 )
            uuencode(bytebuf+i+j,(unsigned char *)sendstr+1+(j/3)*4);
        sendstr[1+((num+2)/3)*4] = 0;
        TargetSendStringAndCR(sendstr);
        buflen = TargetReceiveString(buf, 128, 100);
        if ( strcmp(buf,sendstr) != 0 )
            error = -1;
    }
    // check-sum
    sprintf(sendstr,"%d",chksum);
    TargetSendStringAndCR(sendstr);
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,sendstr) != 0 )
        error = -1;
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,strok) != 0 )
        error = -1;
    
    chksum = 0;
    for ( i = 0 ; i < 512 ; i++ )
         chksum += bytebuf[i+512];
                
    // Write to RAM address 10000400h, 512 bytes
    strcpy(sendstr,"W 268436480 512");
    TargetSendStringAndCR(sendstr);
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,sendstr) != 0 )
        error = -1;
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,str0) != 0 )
        error = -1;



    // uuencode of the second 512 bytes data
    for ( i = 0 ; i < 512 ; i+=45 )
    {
        num = 512 - i;
        if ( num > 45 )
            num = 45;
        sendstr[0] = num + 0x20;
        for ( j = 0 ; j < num ; j+= 3 )
            uuencode(bytebuf+512+i+j,(unsigned char *)sendstr+1+(j/3)*4);
        sendstr[1+((num+2)/3)*4] = 0;
        TargetSendStringAndCR(sendstr);
        buflen = TargetReceiveString(buf, 128, 100);
        if ( strcmp(buf,sendstr) != 0 )
            error = -1;
    }
    sprintf(sendstr,"%d",chksum);
    TargetSendStringAndCR(sendstr);
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,sendstr) != 0 )
        error = -1;
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,strok) != 0 )
        error = -1;
                
    // P 0 17. => P 0 17.0..
    sprintf(sendstr,"P 0 %d",lastsector);
    TargetSendStringAndCR(sendstr);
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,sendstr) != 0 )
        error = -1;
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,str0) != 0 )
        error = -1;
    // copy to flash address (offset) from RAM address 10000200h, 1024 bytes
    sprintf(sendstr,"C %d 268435968 1024",offset);
    TargetSendStringAndCR(sendstr);
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,sendstr) != 0 )
        error = -1;
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,str0) != 0 )
        error = -1;
    return error;
}

int ISP::CheckLine(const int &rectype, const int &addr, const int &reclen) {

        int error = 0;
        if ( rectype == 4 )
        {
            if ( addr != 0 || reclen != 2 ) {
                std::cout << "Invalid Extended Linear Address Record. " <<std::endl;
                error = -10;
                }
        } else if ( rectype == 5 ) {
            if ( addr != 0 || reclen != 4 ) {
                std::cout << "invalid Extended Linear Address Record " <<std::endl;
                error = -9;
                }
        } else if ( rectype == 1 ) {
            if ( addr != 0 || reclen != 0 ) {
                std::cout << " invalid End of File Record" <<std::endl;
                error = -8;
            }
        } else if (rectype == 2 ) {
            if ( addr != 0 || reclen != 2 ) {
                std::cout << " Invalid Extended segment address" <<std::endl;
                error = -11;   
                }     
        } else if ( rectype != 0 ) {
            std::cout << " invalid Record" <<std::endl;
            error = -7;
        }
        

    return error;
}

int ISP::FlashHexFile(char *filename, int lastsector)
{

    if (!synchronized) {
        std::cout << "Target not synchronized!" <<std::endl;
        return -1;
    }
    std::cout << "Flashing Target!" <<std::endl;


    int reclen, addr, rectype, wChkSum, lastaddr=0;
    uint32_t extendedAddr = 0; 
    int i, buflen;
    FILE *fp;
    char buf[128], sendstr[64];
    int bufstart, bufend;
    int error = 0;
    bufstart = 0;
    bufend = bufstart + 1024;

    // Open file
    fp = fopen(filename, "r");
    if (fp == NULL) {
        return -1;
    }

    // Initial Setup for programming

    //U 23130. => U 23130.0..                ; unlock
    TargetSendStringAndCR(strUnlock);
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,strUnlock) != 0 )
        error = -18;
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,str0) != 0 )
        error = -17;
    
    //P 0 17. => P 0 17.0..                    ; prepare sector for write (S,E)
    sprintf(sendstr,"P 0 %d",lastsector);
    TargetSendStringAndCR(sendstr);
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,sendstr) != 0 )
        error = -16;
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,str0) != 0 )
        error = -1;
    //E 0 17. => E 0 17.0..                    ; erase sector (S, E)
    sprintf(sendstr,"E 0 %d",lastsector);
    TargetSendStringAndCR(sendstr);
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,sendstr) != 0 )
        error = -15;
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,str0) != 0 )
        error = -14;
    
    //U 23130. => U 23130.0..                ; unlock
    TargetSendStringAndCR(strUnlock);
    buflen = TargetReceiveString(buf, 128, 100);
    if ( strcmp(buf,strUnlock) != 0 )
        error = -13;
    buflen = TargetReceiveString(buf, 128, 400);
    if ( strcmp(buf,str0) != 0 )
        error = -12;

    for ( i = 0 ; i < 1024 ; i++ )
        bytebuf[i] = 0xFF;
    error = 0;           // ignore previous error


    //Start sending file content
    uint32_t line = 0; 
    float progress = 0;
    while (fgets(buf, sizeof(buf), fp) != NULL) {
         
        progress = (float)++line/numlines;
        printProgressBar(progress);

        if ( buf[0] != ':' ) {
            // invalid start
            error = -11;
        }
        reclen = StrToHex(buf+1,2);
        addr = StrToHex(buf+3,4);
        rectype = StrToHex(buf+7,2);
   
        error = CheckLine(rectype,addr,reclen);
         wChkSum = 0;
        for ( i = 0 ; i < (reclen + 5) ; i++ ) {
            wChkSum += StrToHex(buf+1+i*2, 2);
        }
        if ( (wChkSum & 0xFF) != 0 ) {
            std::cout << "Invalid checksum" <<std::endl;
            error = -6;
        }
        if ( rectype == 0 ) {
            addr += extendedAddr;
            if ( addr < bufstart ) {
                // start address error
                error = -5;
            }
            else {
                for ( i = 0 ; i < reclen ; i++ ) {
                    bytebuf[addr + i-bufstart] = StrToHex(buf+9+i*2, 2);
                    }
            }

            // Increment the lastaddr variable
            if ( lastaddr <= (addr + reclen) ) {
                lastaddr = (addr + reclen);
            } else {
                error = -4;
            }
            if ( lastaddr >= bufend ) {
                if ( ProgramSector(bufstart, bytebuf, lastsector) != 0 ) {
                    error = -3;
                }
                for ( i = 0 ; i < 1024 ; i++ )
                    bytebuf[i] = 0xFF;
                for ( i = bufend ; i < lastaddr ; i++ )
                    bytebuf[i-bufend] = bytebuf[i-bufstart]; // This was bytebuf[i] before, but wrong. Should be fixed
                bufstart += 1024;
                bufend = bufstart + 1024;
            }
        }
        if (rectype == 2 ) {
            extendedAddr = StrToHex(buf+9, 4)<<4;
            //std::cout << "Extended Address: "<<extendedAddr <<std::endl;
        }
    }

    if ( ProgramSector(bufstart, bytebuf, lastsector) != 0 )
        error = -2;
    fclose(fp);
    
    return error;
}

void ISP::TargetSendString(char* in_str){
    int i;

    port.flush();

    for ( i = 0 ; in_str[i] ; i++ ) {
        //std::cout << in_str[i];
        port.write(in_str[i]);
    }

};

void ISP::TargetSendStringAndCR(char* in_str){
    TargetSendString(in_str);
    port.write(0x0D);
};


int ISP::TargetReceiveString(char* strbuf, int bufsize, int timeoutms){
    int bufindex = 0;
    char rch;

    std::clock_t start = std::clock();
    double duration = 0;

    bufsize--;
    strbuf[0] = 0;
    //while (duration < (timeoutms/1000))
    while(1)
    {
            rch = port.read();
            //std::cout << rch;
            if ( rch == 0x0d )
            {
                break;
            }
            if ( bufindex < bufsize )
                strbuf[bufindex++] = rch;
            if ( rch == 0x0a )
                bufindex = 0;

            duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;


    }
    strbuf[bufindex] = 0;
    //std::cout << "Received: '"<<strbuf<<"'" << std::endl;

    return bufindex;
};
