#README

This repository contain the code to program the LPC1768 microcontroller from an ODROID XU4 single board computer using the GPIO pins and the microcontroller bootloader. It was developed as part of the Deep Robot Optical Perception Spheres project at the DROP lab, University of Michigan.

## Code 
The ISP programming class is implemented as a abstract class with two functions that need to be implemented by any derived class: the resetTarget function, which resets the LPC1768 microcontroller, and the Target2ISP() function, that resets the LPC1768 while enabling the bootloader pin. The default blank implementation leaves the reset and bootloader enabling up to the user, the ODROID version uses two GPIO pins to do the reset and bootloader steps automatically. 

It also includes a script to convert the *.bin files from the mbed compiler to .hex files required by the programmer. 

## Compiling the code 
Clone the repository and make a "build" folder inside of it. Run cmake .. from inside the build folder, followed by make. If you are compiling on the ODROID and have WiringPi installed, use "cmake -DODROID=ON .." and then run make. 

## Physical Interface
If using the odroid, the boot pin is assumed to be connected to odroid pin 2, while the reset pin to odroid pin 7. This can however be easily changed in the code. A voltage level shifter is also required to connect the 1.8V Odroid lines to the 3.3V LPC1768. 

