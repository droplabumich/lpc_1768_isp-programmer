from __future__ import division
from __future__ import print_function
import subprocess as sp
import time
import os 

sw1_pin = 3  
sw2_pin = 22
filepath = "/home/odroid/odroid-development/lpc_1768_isp-programmer/binaries/mbed-os-allsensors.bin"


current_path = os.path.abspath(__file__)
(current_path, _) = os.path.split(current_path)
(root, ext)= os.path.splitext(filepath)

print(root)
print(current_path)
if ext =='.bin':
	cmd = 'arm-linux-gnueabihf-objcopy -I binary -O ihex '+filepath+" "+root+".hex"
	return_code = sp.call(cmd, shell=True)  
	filepath=root+'.hex'

#Set MUX pins to direct serial to LPC1768 (3=0,22=0)
cmd = "gpio mode "+str(sw1_pin)+" out"
return_code = sp.call(cmd, shell=True) 
cmd = "gpio mode "+str(sw2_pin)+" out"
return_code = sp.call(cmd, shell=True) 
cmd = "gpio write "+str(sw1_pin)+" 0"
return_code = sp.call(cmd, shell=True) 
cmd = "gpio write "+str(sw2_pin)+" 0"
return_code = sp.call(cmd, shell=True) 

program_cmd = 'sudo /home/odroid/odroid-development/lpc_1768_isp-programmer/build/LPC_1768_Programmer -f '+filepath+' -s 115200 -p /dev/ttySAC0'
return_code = sp.call(program_cmd, shell=True) 
