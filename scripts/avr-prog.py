from __future__ import division
from __future__ import print_function
import subprocess as sp
import time

avr_reset_pin = 5; 
sw1_pin = 3  
sw2_pin = 22
value = 0
filepath = '/home/odroid/odroid-development/lpc_1768_isp-programmer/binaries/led_ui.ino.hex'
# 

#Set MUX pins to direct serial to LPC1768 (3=0,22=0)
cmd = "gpio mode "+str(sw1_pin)+" out"
return_code = sp.call(cmd, shell=True) 
cmd = "gpio mode "+str(sw2_pin)+" out"
return_code = sp.call(cmd, shell=True) 
cmd = "gpio write "+str(sw1_pin)+" 0" #0
return_code = sp.call(cmd, shell=True) 
cmd = "gpio write "+str(sw2_pin)+" 1" #1
return_code = sp.call(cmd, shell=True) 


#Set the reset pin as an output
output_command = "gpio mode "+str(avr_reset_pin)+" out"
_proc = sp.Popen("exec "+ output_command, shell=True)
#Set the pin low
write_command = "gpio write "+str(avr_reset_pin)+" "+str(value)
_proc = sp.Popen("exec "+ write_command, shell=True)

time.sleep(3)

#Set the reset pin as input and enable pull-up resistor
input_command = "gpio mode "+str(avr_reset_pin)+" in"
_proc = sp.Popen("exec "+ input_command, shell=True)
pullup_command = "gpio mode "+str(avr_reset_pin)+" up"
_proc = sp.Popen("exec "+ pullup_command, shell=True)

#
avr_dude_command = "avrdude-original -v -p atmega328p -c arduino -P /dev/ttySAC0 -b 115200 -D -U flash:w:"+filepath+":i"
_proc = sp.Popen("exec "+ avr_dude_command, shell=True)

