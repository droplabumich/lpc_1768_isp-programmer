find_library(WIRINGPI_LIBRARIES wiringPi /home/odroid/odroid-development/wiringPi)
find_path(WIRINGPI_INCLUDE_DIRS wiringPi /home/odroid/odroid-development/wiringPi.h) 

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(wiringPi DEFAULT_MSG WIRINGPI_LIBRARIES WIRINGPI_INCLUDE_DIRS)
